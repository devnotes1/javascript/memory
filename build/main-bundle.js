/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://code-wars/./src/scss/style.scss?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../scss/style.scss */ \"./src/scss/style.scss\");\n\r\n\r\nconst container = document.querySelector('.playing-field');\r\nconst btn = document.querySelector('button');\r\nconst firstPage = document.querySelector('.start-game');\r\nconst select = document.querySelector('#select');\r\n\r\nlet items = [\r\n\t{ name: 'lion', img: './publick/lion.png' },\r\n\t{ name: 'lion', img: './publick/lion.png' },\r\n\t{ name: 'pig', img: './publick/pig.png' },\r\n\t{ name: 'pig', img: './publick/pig.png' },\r\n\t{ name: 'unicorn', img: './publick/unicorn.png' },\r\n\t{ name: 'unicorn', img: './publick/unicorn.png' },\r\n\t{ name: 'cat', img: './publick/cat.png' },\r\n\t{ name: 'cat', img: './publick/cat.png' },\r\n\t{ name: 'chicken', img: './publick/chicken.png' },\r\n\t{ name: 'chicken', img: './publick/chicken.png' },\r\n\t{ name: 'birds', img: './publick/birds.png' },\r\n\t{ name: 'birds', img: './publick/birds.png' },\r\n\t{ name: 'panda', img: './publick/panda.png' },\r\n\t{ name: 'panda', img: './publick/panda.png' },\r\n\t{ name: 'cow', img: './publick/cow.png' },\r\n\t{ name: 'cow', img: './publick/cow.png' },\r\n\t{ name: 'squirrel', img: './publick/squirrel.png' },\r\n\t{ name: 'squirrel', img: './publick/squirrel.png' },\r\n];\r\n\r\nlet activeCard = '';\r\nlet activeCards = [];\r\nlet steams = 0;\r\nlet round = 0;\r\nlet startTime;\r\nlet level;\r\nlet field = 0;\r\nlet cardsAnimals;\r\n\r\n//Do porawy\r\nconst playingField = function () {\r\n\tswitch (level) {\r\n\t\tcase 'easy':\r\n\t\t\tcardsAnimals.length = 6;\r\n\t\t\tround = cardsAnimals.length / 2;\r\n\t\t\tcontainer.classList.add('card-container--small');\r\n\t\t\tbreak;\r\n\t\tcase 'medium':\r\n\t\t\tcardsAnimals.length = 12;\r\n\t\t\tround = cardsAnimals.length / 2;\r\n\t\t\tcontainer.classList.add('card-container--medium');\r\n\t\t\tbreak;\r\n\t\tcase 'hard':\r\n\t\t\tround = cardsAnimals.length / 2;\r\n\t\t\tcontainer.classList.add('card-container--large');\r\n\t\t\tbreak;\r\n\t}\r\n};\r\n\r\n//silnik gry :)\r\nconst showCard = function () {\r\n\tactiveCard = this;\r\n\tif (activeCards.length == 0) {\r\n\t\tactiveCards[0] = activeCard;\r\n\t\tthis.classList.add('flipper');\r\n\t} else if (activeCards.length == 1) {\r\n\t\tthis.classList.add('flipper');\r\n\t\tactiveCards[1] = activeCard;\r\n\t\tactiveCards[0].removeEventListener('click', showCard);\r\n\t\tactiveCards[1].removeEventListener('click', showCard);\r\n\t\tif (activeCards[0].dataset.name == activeCards[1].dataset.name) {\r\n\t\t\tsetTimeout(() => {\r\n\t\t\t\tthis.innerHTML = '';\r\n\t\t\t\tactiveCards[0].innerHTML = '';\r\n\t\t\t\tactiveCards.length = 0;\r\n\t\t\t\tactiveCard = '';\r\n\t\t\t\tsteams++;\r\n\r\n\t\t\t\tif (steams == round) {\r\n\t\t\t\t\tlet endTime = new Date().getTime();\r\n\t\t\t\t\talert('Wygrana!!! czas:' + Math.floor((endTime - startTime) / 1000) + 'sekund');\r\n\t\t\t\t\trestartGame();\r\n\t\t\t\t}\r\n\t\t\t}, '2000');\r\n\t\t} else {\r\n\t\t\tsetTimeout(() => {\r\n\t\t\t\tthis.classList.remove('flipper');\r\n\t\t\t\tactiveCards[0].classList.remove('flipper');\r\n\t\t\t\tactiveCards[0].addEventListener('click', showCard);\r\n\t\t\t\tactiveCards[1].addEventListener('click', showCard);\r\n\t\t\t\tactiveCards.length = 0;\r\n\t\t\t\tactiveCard = '';\r\n\t\t\t}, '2000');\r\n\t\t}\r\n\t}\r\n};\r\n\r\n//funkcja tworząca elementy\r\nconst createCards = function () {\r\n\tfield = cardsAnimals.length - 1;\r\n\tfor (let i = 0; i <= field; i++) {\r\n\t\tconst number = Math.floor(Math.random() * cardsAnimals.length);\r\n\t\tconst cardContainer = document.createElement('div');\r\n\t\tconst cardBefore = document.createElement('div');\r\n\t\tconst cardAfter = document.createElement('div');\r\n\t\tconst img = document.createElement('img');\r\n\t\tconst questionMark = document.createElement('img');\r\n\r\n\t\tcardContainer.classList.add('card-container');\r\n\t\tcardContainer.setAttribute('data-name', cardsAnimals[number].name);\r\n\t\tcardBefore.classList.add('card-before');\r\n\t\tcardAfter.classList.add('card-after');\r\n\t\timg.classList.add('start-game__img');\r\n\t\timg.setAttribute('src', `${cardsAnimals[number].img}`);\r\n\t\tquestionMark.classList.add('start-game__img');\r\n\t\tquestionMark.setAttribute('src', './publick/question-mark.png');\r\n\r\n\t\tcontainer.appendChild(cardContainer);\r\n\t\tcardContainer.appendChild(cardAfter);\r\n\t\tcardContainer.appendChild(cardBefore);\r\n\t\tcardAfter.appendChild(img);\r\n\t\tcardBefore.appendChild(questionMark);\r\n\r\n\t\tcardContainer.addEventListener('click', showCard);\r\n\r\n\t\tcardsAnimals.splice(number, 1);\r\n\t}\r\n};\r\n\r\n//funkcja sterująca\r\nconst init = function () {\r\n\tstartTime = new Date().getTime();\r\n\tfirstPage.style.display = 'none';\r\n\tlevel = select.value;\r\n\tcardsAnimals = [...items];\r\n\tplayingField();\r\n\tcreateCards();\r\n};\r\n\r\nconst restartGame = function () {\r\n\tfirstPage.style.display = 'block';\r\n\tcontainer.innerHTML = '';\r\n\tsteams = 0;\r\n\tround = 0;\r\n\tlevel = '';\r\n};\r\n\r\nbtn.addEventListener('click', init);\r\n\n\n//# sourceURL=webpack://code-wars/./src/js/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/index.js");
/******/ 	
/******/ })()
;