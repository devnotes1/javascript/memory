const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	mode: 'production',
	entry: {
		main: './js/index.js',
	},
	output: {
		filename: '[name]-[contenthash:6]-bundle.js',
		path: path.resolve(__dirname, '../', 'build'),
	},
	module: {
		rules: [
			{
				test: /\.txt$/,
				use: 'raw-loader',
			},
			{
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			},
			{
				test: /\.(sass|scss)$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
			},
		],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			title: 'stronka bez końca',
			template: 'index.html',
			filename: 'index-bundle.html',
		}),
		new MiniCssExtractPlugin({
			filename: '[name]-[contenthash:6]-bundle.css',
		}),
	],
};
