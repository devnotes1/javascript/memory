import '../scss/style.scss';

const container = document.querySelector('.playing-field');
const btn = document.querySelector('button');
const firstPage = document.querySelector('.start-game');
const select = document.querySelector('#select');

let items = [
	{ name: 'lion', img: './publick/lion.png' },
	{ name: 'lion', img: './publick/lion.png' },
	{ name: 'pig', img: './publick/pig.png' },
	{ name: 'pig', img: './publick/pig.png' },
	{ name: 'unicorn', img: './publick/unicorn.png' },
	{ name: 'unicorn', img: './publick/unicorn.png' },
	{ name: 'cat', img: './publick/cat.png' },
	{ name: 'cat', img: './publick/cat.png' },
	{ name: 'chicken', img: './publick/chicken.png' },
	{ name: 'chicken', img: './publick/chicken.png' },
	{ name: 'birds', img: './publick/birds.png' },
	{ name: 'birds', img: './publick/birds.png' },
	{ name: 'panda', img: './publick/panda.png' },
	{ name: 'panda', img: './publick/panda.png' },
	{ name: 'cow', img: './publick/cow.png' },
	{ name: 'cow', img: './publick/cow.png' },
	{ name: 'squirrel', img: './publick/squirrel.png' },
	{ name: 'squirrel', img: './publick/squirrel.png' },
];

let activeCard = '';
let activeCards = [];
let steams = 0;
let round = 0;
let startTime;
let level;
let field = 0;
let cardsAnimals;

//Do porawy
const playingField = function () {
	switch (level) {
		case 'easy':
			cardsAnimals.length = 6;
			round = cardsAnimals.length / 2;
			container.classList.add('card-container--small');
			break;
		case 'medium':
			cardsAnimals.length = 12;
			round = cardsAnimals.length / 2;
			container.classList.add('card-container--medium');
			break;
		case 'hard':
			round = cardsAnimals.length / 2;
			container.classList.add('card-container--large');
			break;
	}
};

//silnik gry :)
const showCard = function () {
	activeCard = this;
	if (activeCards.length == 0) {
		activeCards[0] = activeCard;
		this.classList.add('flipper');
	} else if (activeCards.length == 1) {
		this.classList.add('flipper');
		activeCards[1] = activeCard;
		activeCards[0].removeEventListener('click', showCard);
		activeCards[1].removeEventListener('click', showCard);
		if (activeCards[0].dataset.name == activeCards[1].dataset.name) {
			setTimeout(() => {
				this.innerHTML = '';
				activeCards[0].innerHTML = '';
				activeCards.length = 0;
				activeCard = '';
				steams++;

				if (steams == round) {
					let endTime = new Date().getTime();
					alert('Wygrana!!! czas:' + Math.floor((endTime - startTime) / 1000) + 'sekund');
					restartGame();
				}
			}, '2000');
		} else {
			setTimeout(() => {
				this.classList.remove('flipper');
				activeCards[0].classList.remove('flipper');
				activeCards[0].addEventListener('click', showCard);
				activeCards[1].addEventListener('click', showCard);
				activeCards.length = 0;
				activeCard = '';
			}, '2000');
		}
	}
};

//funkcja tworząca elementy
const createCards = function () {
	field = cardsAnimals.length - 1;
	for (let i = 0; i <= field; i++) {
		const number = Math.floor(Math.random() * cardsAnimals.length);
		const cardContainer = document.createElement('div');
		const cardBefore = document.createElement('div');
		const cardAfter = document.createElement('div');
		const img = document.createElement('img');
		const questionMark = document.createElement('img');

		cardContainer.classList.add('card-container');
		cardContainer.setAttribute('data-name', cardsAnimals[number].name);
		cardBefore.classList.add('card-before');
		cardAfter.classList.add('card-after');
		img.classList.add('start-game__img');
		img.setAttribute('src', `${cardsAnimals[number].img}`);
		questionMark.classList.add('start-game__img');
		questionMark.setAttribute('src', './publick/question-mark.png');

		container.appendChild(cardContainer);
		cardContainer.appendChild(cardAfter);
		cardContainer.appendChild(cardBefore);
		cardAfter.appendChild(img);
		cardBefore.appendChild(questionMark);
		cardContainer.addEventListener('click', showCard);
		cardsAnimals.splice(number, 1);
	}
};

//funkcja sterująca
const init = function () {
	startTime = new Date().getTime();
	firstPage.style.display = 'none';
	level = select.value;
	cardsAnimals = [...items];
	playingField();
	createCards();
};

const restartGame = function () {
	firstPage.style.display = 'block';
	container.innerHTML = '';
	steams = 0;
	round = 0;
	level = '';
};

btn.addEventListener('click', init);
